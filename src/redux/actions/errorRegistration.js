export const SET_ERROR = () => {
    return {
        type: "SET_ERROR"
    }
}

export const SET_SUCCESS = () => {
    return {
        type: "SET_SUCCESS"
    }
}

export const CLEAR_ERROR = () => {
    return {
        type: "CLEAR_ERROR"
    }
}