export const setLoaderTrue = () => ({
    type: 'SET_LOADING_TRUE',
    payload: "set true"
})

export const setLoaderFalse = () => ({
    type: 'SET_LOADING_FALSE',
    payload: "set false"
})