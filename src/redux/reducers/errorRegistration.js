const initialState = {
    error: false,
    success: false
}

const errorRegistration = (state = initialState, action) => {
    switch (action.type) {
        case "SET_ERROR":
            return {
                success: false,
                error: true
            }
        case "SET_SUCCESS":
            return {
                error: false,
                success: true
            }
        case "CLEAR_ERROR":
            return initialState
        default: return state
    }
}
export default errorRegistration;