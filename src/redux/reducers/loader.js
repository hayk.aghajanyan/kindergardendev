const initialState = {
    loading: false,
    message: null
}

const loader = (state = initialState, action ) => {
    switch (action.type) {
        case 'SET_LOADING_TRUE':
            return {
                ...state,
                loading: true,
                message: action.payload
            }
        case 'SET_LOADING_FALSE':
            return {
                ...state,
                loading: false,
                message: action.payload
            }
        default:
            return state
    }
}

export default loader;