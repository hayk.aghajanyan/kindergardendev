import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import loader from "./loader";
import errorRegistration from "./errorRegistration";

const rootReducer = combineReducers({
    form: formReducer,
    loader,
    errorRegistration
});

export default rootReducer;