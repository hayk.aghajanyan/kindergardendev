import React, { useEffect } from "react";
import { Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { theme } from "../theme";
import RecipesTable from "../RecipesTable";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles({
    wrapper: {
        background: "url('https://w3.chabad.org/media/images/1020/cxGh10202712.jpg')",
        minHeight: "calc(100vh - 70px)"
    },
    title: {
        fontFamily: theme.typography.fontFamily[3],
        padding: theme.spacing(5, 0 , 5),
        margin: theme.spacing(5, 0, 5),
        color: theme.palette.common.white,
        textAlign: "center",
        position: "relative",
        boxShadow: "0 2px 22px rgba(102, 51, 153, 0.5)",
        borderRadius: "120px",
        transition: "background 0.3s linear",
        "&::after": {
            content: "''",
            display: "block",
            position: "absolute",
            top: "80%",
            right: "12%",
            height: "3px",
            width: "75%",
            borderRadius: "5px",
            backgroundColor: theme.palette.common.white
        },
        "&::before": {
            content: "''",
            display: "block",
            position: "absolute",
            top: "25%",
            right: "12%",
            height: "2px",
            width: "75%",
            borderRadius: "5px",
            backgroundColor: theme.palette.common.white
        },
        "&:hover": {
            background: "linear-gradient(180deg, rgba(217, 0, 18, 0.66) 0%, rgba(0, 51, 160,0.98) 58%, rgba(242, 168, 0,0.78) 80%, rgba(235,183,42,0.73) 100%)",
            transition: "0.3s linear",
            cursor: "default"
        },
        '&::selection': {
            background: "transparent"
        }
    },
    table: {
        minWidth: 700,
    },
})

const Special = () => {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 655px)");

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <div className={classes.wrapper}>
            <Container maxWidth="xs" style={matches ? {width: "250px"} : null}>
                <Typography className={classes.title}
                            id="title"
                            variant={matches? "h4" : "h2"}>
                    Ճաշացանկ
                </Typography>
            </Container>
            <RecipesTable />
        </div>
    )
}

export default Special;