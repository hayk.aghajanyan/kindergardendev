import React, {useEffect, useRef, useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import { NavLink } from "react-router-dom";
import ReorderRoundedIcon from '@material-ui/icons/ReorderRounded';
import logo from "../../assets/img/Alladin.png";
import Grid from "@material-ui/core/Grid";
import { theme } from "../theme";

const navigationSet = [
    {
        title: "home",
        to: "/"
    },
    {
        title: "special",
        to: "/special"
    },
    {
        title: "about",
        to: "/about"
    },
    {
        title: "registration",
        to: "/registration"
    }
];

const useStyles = makeStyles({
    appBar: {
        height: "80px",
        zIndex: 1000
    },
    navBar: {
        display: "flex",
    },
    TypographyNavLink: {
        marginRight: "25px",
    },
    navLink: {
        textDecoration: "none",
        textTransform: "uppercase",
        margin: "0 auto",
        width: "100%",
        color: '#000000',
        fontFamily: theme.typography.fontFamily[4],
        fontSize: 20,
        letterSpacing: 5,
        fontWeight: 400,
        '&:hover': {
            color: theme.palette.primary.dark,
            transition: "0.2s"
        },
        '&::selection': {
            background: "transparent"
        },
    },
    active: {
        color: theme.palette.primary.dark,
    },
    icon: {
        width: "100px",
        verticalAlign: "bottom"
    },
    altNavbar: {
        position: "fixed",
        width: "100%",
        backgroundColor: theme.palette.primary.main,
        zIndex: 999,
        transform: "translateY(-100%)",
        transition: "transform 0.3s ease-in-out"
    },
    activeNavbar: {
        transform: "translateY(0)",
    },
    pd5: {
        padding: "1.5rem 0"
    }
})

const Header = () => {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 791px)");
    const [toolbarClass, setToolbarClass] = useState(null);
    const closeRef = useRef(),
          hamburgerRef = useRef();

    const toggleNavbar = () => {
        if (!toolbarClass) {
            setToolbarClass(classes.activeNavbar);
        } else {
            setToolbarClass(null)
        }
    };

    const closeNavbar = (e) => {
        if(!e.path.includes(closeRef.current) && !e.path.includes(hamburgerRef.current)) {
            setToolbarClass(null);
        }
    };

    useEffect(() => {
        document.body.addEventListener("click", closeNavbar);
    }, []);

    return (
        <>
            <AppBar className={classes.appBar} color="primary" position="sticky">
                <Toolbar>
                    <Grid container alignItems={"center"}>
                        <Grid item xs={2}>
                            <IconButton onClick={() => {setToolbarClass(null)}}>
                                <NavLink to="/" exact>
                                    <img className={classes.icon} src={logo} alt="#"/>
                                </NavLink>
                            </IconButton>
                        </Grid>
                        {
                            !matches && <Grid item xs={false} sm={false} md={1}/>
                        }
                        {
                            !matches &&
                            <Grid xs={9} sm={10} md={9} className={classes.navBar} justify={"space-evenly"} container
                                  item>
                                <Grid className={classes.TypographyNavLink}>
                                    <NavLink className={classes.navLink}
                                             activeClassName={classes.active}
                                             to="/"
                                             exact
                                    >Home</NavLink>
                                </Grid>
                                <Grid className={classes.TypographyNavLink}>
                                    <NavLink className={classes.navLink}
                                             activeClassName={classes.active}
                                             to="/special"
                                    >Special</NavLink>
                                </Grid>
                                <Grid className={classes.TypographyNavLink}>
                                    <NavLink className={classes.navLink}
                                             activeClassName={classes.active}
                                             to="/about"
                                    >About</NavLink>
                                </Grid>
                                <Grid className={classes.TypographyNavLink}>
                                    <NavLink className={classes.navLink}
                                             activeClassName={classes.active}
                                             to="/registration"
                                    >Registration</NavLink>
                                </Grid>
                            </Grid>
                        }
                        {
                            matches && <Grid xs={8} item/>
                        }
                        {
                            matches && <Grid xs={2} item>
                                <IconButton ref={hamburgerRef} onClick={toggleNavbar}>
                                    <ReorderRoundedIcon fontSize={"large"}/>
                                </IconButton>
                            </Grid>
                        }
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar ref={closeRef} className={`${classes.altNavbar} ${toolbarClass}`}>
                <Grid className={classes.pd5} direction={"column"} spacing={3} justify={"space-around"}
                      alignItems={"center"} container>
                    {
                        navigationSet.map((item) => {
                            return <Grid xs={12} key={item.title} item>
                                <NavLink className={classes.navLink}
                                         activeClassName={classes.active}
                                         to={item.to}
                                         onClick={toggleNavbar}
                                         exact>
                                    {item.title}
                                </NavLink>
                            </Grid>
                        })
                    }
                </Grid>
            </Toolbar>
        </>
    )
}

export default Header;