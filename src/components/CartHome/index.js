import React from "react";
import { CardActionArea, Grid } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
import CardMedia from "@material-ui/core/CardMedia";
import Fade from "react-reveal";
import {Link} from "react-router-dom";
import {theme} from "../theme";

const useStyles = makeStyles({
    card: {
        display: "flex",
        backgroundColor: "#8dd6a1",
        cursor: "default"
    },
    cardDetails: {
        flex: 1,
    },
    cardMedia: {
        width: "160px"
    },
    center: {
        textAlign: "center",
        margin: "10px auto",
        boxShadow: "-2px 2px 37px -11px rgba(102, 51, 153, 0.5)",
    },
    link: {
        textDecoration: "none",
        color: theme.palette.common.white
    }
})

const CartHome = ({left, right, title, content, image, link}) => {
    const classes = useStyles();

    return (
        <Fade left={left} right={right}>
            <Grid className={classes.center} item xs={10} md={6}>
                <CardActionArea>
                    <Card className={classes.card}>
                        {
                            left && <Hidden xsDown>
                                <CardMedia className={classes.cardMedia} image={image} title={"Title"} />
                            </Hidden>
                        }
                        <div className={classes.cardDetails}>
                            <CardContent>
                                <Typography component="h2" variant="h5">
                                    {title}
                                </Typography>
                                <Typography variant="subtitle1" paragraph>
                                    {content}
                                </Typography>
                                <Typography variant="subtitle1" color="primary">
                                    {
                                        link ? <Link className={classes.link} to={link}>
                                            Continue reading...
                                        </Link>
                                            : null
                                    }
                                </Typography>
                            </CardContent>
                        </div>
                        {
                            right && <Hidden xsDown>
                                <CardMedia className={classes.cardMedia} image={image} title={"Title"} />
                            </Hidden>
                        }
                    </Card>
                </CardActionArea>
            </Grid>
        </Fade>
    )
}

export default CartHome;