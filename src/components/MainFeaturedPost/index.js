import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import useMediaQuery from "@material-ui/core/useMediaQuery";
import image from "../../assets/img/nkar1.jpg"
import {theme} from "../theme";

const useStyles = makeStyles((theme) => ({
    mainFeaturedPost: {
        position: 'relative',
        fontFamily: theme.typography.fontFamily[3],
        backgroundColor: theme.palette.grey[800],
        color: theme.palette.common.white,
        marginBottom: theme.spacing(4),
        backgroundImage: `url(${image})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        boxShadow: "0 2px 22px rgba(102, 51, 153, 0.5)",
    },
    overlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: 'rgba(0,0,0,.3)',
    },
    mainFeaturedPostContent: {
        position: 'relative',
        padding: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(6),
            paddingRight: 0,
        },
    },
    title: {
        fontSize: "79px",
        "&:hover": {
            color: theme.palette.secondary.light,
            transition: "0.3s linear",
        },
        '&::selection': {
            background: "transparent"
        }
    },
    smallTitle: {
        fontSize: "3rem",
        paddingBottom: "15%",
        "&:hover": {
            color: theme.palette.secondary.light,
            transition: "0.3s linear",
        },
        '&::selection': {
            background: "transparent"
        }
    },
    adaptiveFont: {
        fontSize: "2rem",
        '&::selection': {
            background: "transparent"
        }
    },
    smallAdaptiveFont: {
        fontSize: "1.3rem",
        '&::selection': {
            background: "transparent"
        }
    },
    link: {
        color: theme.palette.common.white,
        textDecoration: "none"
    }
}));

export default function MainFeaturedPost() {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 678px)");
    const smallMatches = useMediaQuery("(max-width: 500px)");

    return (
        <Paper className={classes.mainFeaturedPost} >
            <div className={classes.overlay} />
            <Grid container>
                <Grid item md={6}>
                    <div className={classes.mainFeaturedPostContent}>
                        <Typography className={matches? smallMatches? classes.smallAdaptiveFont : classes.adaptiveFont : null} style={{cursor: "default"}} component="h1" variant="h3" color="inherit" gutterBottom>
                            <span className={matches? smallMatches? classes.adaptiveFont : classes.smallTitle : classes.title}>Aladdin Kids Club</span> մանկապարտեզում 1,5-6 տարեկան երեխաների ընդունելությունը սկսված է
                        </Typography>
                        <Typography style={{cursor: "default", fontFamily: theme.typography.fontFamily['typeface-fredericka-the-great']}} variant="h5" color="inherit" paragraph>
                            Շաբաթական 5 օր ` երկուշաբթիից մինչև ուրբաթ
                        </Typography>
                        <Link variant="h6" className={classes.link} to={"/registration"}>
                            ժամերը՝ 08:30-18:30-ը կամ 08։30-14։00-ը
                        </Link>
                    </div>
                </Grid>
            </Grid>
        </Paper>
    );
}
