import React, {useCallback, useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import classNames from "classnames"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const useStyles = makeStyles({
    slider: {
        position: "relative",
        width: "100%",
        height: "100vh",
        boxSizing: "border-box",
        margin: 0,
        padding: 0,
        display: "flex",
        alignItems: "center",
        overflow: "hidden"
    },
    slide: {
        minWidth: "100%",
        height: "80vh",
        transition: "0.5s",
        overflow: "hidden"
    },
    btn: {
        position: "absolute",
        top: "50%",
        transform: "translateY(-50%)",
        width: "10%",
        height: "80%",
        background: "none",
        border: "none",
        outline: "none",
        transition: "0.5s",
        "&:hover": {
            background: "rgba(0, 0, 0, 0.33)",
            cursor: "pointer",
            color: "#f5f5f5"
        }
    },
    left: {
        left: 0
    },
    right: {
        right: 0
    }
})

const Slider = ({items}) => {
    const classes = useStyles()
    const [x, setX] = useState(0)

    const goLeft = () => {
        x === 0 ? setX(-100 * (items.length - 1)) : setX(x + 100)
    }
    const goRight = useCallback(() => {
        x === -100 * (items.length - 1) ? setX(0) : setX(x - 100)
    }, [x, items])

    useEffect(() => {
        const interval = setInterval(() => {
            goRight()
        }, 3000)
        return () => clearInterval(interval)
    }, [goRight])

    return (
        <div className={classes.slider}>
            {
                items.map((item) => {
                    return (
                        <img src={item.src}
                             key={item.id}
                             className={classes.slide}
                             style={{transform: `translateX(${x}%)`}}
                             alt={"#"}
                        />
                    )
                })
            }
            <button onClick={goLeft} className={classNames(classes.btn, classes.left)}>
                <FontAwesomeIcon icon={['fas', 'fa-chevron-left']} />
            </button>
            <button onClick={goRight} className={classNames(classes.btn, classes.right)}>
                <FontAwesomeIcon icon={['fas', 'fa-chevron-right']} />
                {/*<i className="fas fa-chevron-right"> </i>*/}
            </button>
        </div>
    )
}

export default Slider;