import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Loader = () => {
    return (
        <div>
            <CircularProgress style={{position: "absolute", top: "55%", right: "48%"}} color="secondary"/>
        </div>
    )
}

export default Loader;