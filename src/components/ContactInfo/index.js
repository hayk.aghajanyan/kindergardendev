import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { YMaps, Map } from "react-yandex-maps";
import { Grid } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ContactsListItem from "../ContactsListItem";
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { theme } from "../theme";

const useStyles = makeStyles({
    container: {
        height: "250px",
        width: "100%",
    },
    centered: {
        fontFamily: theme.typography.fontFamily[3],
        textAlign: "center",
        margin: "3% auto 3%",
        color: theme.palette.common.white,
        cursor: "default",
        '&::selection': {
            background: "transparent"
        },
    },
    colored: {
        backgroundColor: theme.palette.primary.light,
        paddingBottom: "3%"
    },
    divider: {
        height: "1px",
        backgroundColor: "#ffffff",
    },
    rounded: {
        borderRadius: "5px"
    },
    title: {
        textAlign: "center",
    },
    subTitle: {
        textAlign: "center",
        margin: "3% auto 3%",
        color: theme.palette.common.white,
        position: "relative",
        cursor: "default",
        "&::after": {
            content: "''",
            display: "block",
            position: "absolute",
            top: "114%",
            right: "25%",
            width: "49%",
            height: "2px",
            backgroundColor: "#ffffff"
        },
        '&::selection': {
            background: "transparent"
        },
    },
    mBot: {
        marginBottom: "5%",
        backgroundColor: "#e3e3e3"
    },
    mt: {
        marginTop: "2%"
    }
})

const ContactInfo = () => {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 1200px)")

    return (
        <Grid className={classes.colored} container>
            <Grid container item>
                <Grid xs={1} sm={2} md={3} item/>
                <Grid item xs={10} sm={8} md={6}>
                    <Typography variant="h2" className={classes.centered}>
                        Our Location
                    </Typography>
                </Grid>
                <Grid xs={1} sm={2} md={3} item/>
            </Grid>

            {
                !matches &&
                <Grid justify={"space-between"} alignItems={"center"} container item>
                    <Grid xs={false} sm={1} item/>
                    <Grid xs={12} sm={5} item>
                        <YMaps>
                            <div>
                                <Map className={classes.container}
                                     defaultState={{center: [40.194468, 44.475473], zoom: 15}}/>
                            </div>
                        </YMaps>
                    </Grid>
                    <Grid xs={false} sm={1} item/>
                    <Grid xs={12} sm={4} item>
                        <Grid justify={"center"} xs={12} item container>
                            <Grid xs={12} item>
                                <Typography variant="h4" className={classes.subTitle}>
                                    Contacts us
                                </Typography>
                            </Grid>
                            <List>
                                <ContactsListItem/>
                            </List>
                        </Grid>
                    </Grid>
                    <Grid xs={false} sm={1} item/>
                </Grid>
            }

            {
                matches &&
                <Grid container item>
                    <Grid xs={1} sm={2} md={3} item/>
                    <Grid xs={10} sm={8} md={6} item>
                        <YMaps>
                            <div>
                                <Map className={classes.container}
                                     defaultState={{center: [40.194468, 44.475473], zoom: 15}}/>
                            </div>
                        </YMaps>
                    </Grid>
                    <Grid xs={1} sm={2} md={3} item/>
                </Grid>
            }
            {
                matches &&
                <Grid className={classes.mt} justify={"center"} container item>
                    <Grid xs={1} item/>
                    <Grid xs={10} item>
                        <Typography variant="h6" className={classes.subTitle}>
                            Our Contacts
                        </Typography>
                        <ContactsListItem style={{alignItems: "center"}} />
                    </Grid>
                    <Grid xs={1} item/>
                </Grid>
            }
        </Grid>
    )
}

export default ContactInfo;