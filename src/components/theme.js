import { createMuiTheme } from "@material-ui/core";

const titleFont = "'Fredericka the Great', cursive";
const navFont = "'Architects Daughter', cursive";
const exo = "'Exo', sans-serif";

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#fff7f5',
            light: '#663399',
            dark: '#ebb72a',
        },
        secondary: {
            main: '#50355e',
            light: '#b80d0d',
            dark: '#c45d37',
        },
        background: {
            default: "#ffac36",
        }
    },
    typography: {
        fontFamily: [
            'Helvetica Neue',
            'Ubuntu',
            'Futura PT',
            titleFont,
            navFont,
            'typeface-benchnine',
            'sans-serif',
            'typeface-fredericka-the-great',
            exo,
        ]
    }
})