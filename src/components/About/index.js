import React, {useEffect, useState} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import Fade from "react-reveal";
import { fetcher} from "../../helpers/fetcher";
import Loader from "../Loader";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import red from "@material-ui/core/colors/red";
import randomImage from "../../assets/img/nkar1.jpg"


const useStyles = makeStyles((theme) => ({
    wrapper: {
        background: "url('https://w3.chabad.org/media/images/1020/cxGh10202712.jpg')",
    },
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.primary.light,
        padding: theme.spacing(8, 0, 6),
        boxShadow: "-2px 10px 56px -5px rgba(102, 51, 153, 0.5)",
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '80%', // 16:9 56.25%
    },
    cardContent: {
        flexGrow: 1,
    },
    link: {
        textDecoration: "none",
        color: theme.palette.secondary
    },
    shadow: {
        boxShadow: "0px 0px 40px -17px rgba(0,0,0,0.3)"
    },
    title: {
        textAlign: "center",
        color: theme.palette.primary.dark,
        fontFamily: theme.typography.fontFamily[3],
        cursor: "default",
        '&::selection': {
            background: "transparent"
        }
    },
    root: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    avatar: {
        backgroundColor: red[500],
    },
    container: {
        marginTop: 30,
        marginLeft: 30,
    },
    header: {
        fontSize: 10
    },
    text: {
        fontSize: 15,
        fontWeight: "bold",
        color: "black"
    },
    text1: {
        marginLeft: 10
    }
}));

const About = () => {
    const classes = useStyles();
    const [teachers, setTeachers] = useState()

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    useEffect(() => {
        fetcher('/teacherregister').then((data) => setTeachers(data));
    }, [])

    if (!teachers) {
        return <Loader/>
    } else {
        return (
            <div className={classes.wrapper}>
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1"
                                    variant="h2"
                                    className={classes.title}
                                    gutterBottom>
                            Our personal
                        </Typography>
                        <Typography variant="h5"
                                    className={classes.title}
                                    paragraph>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium ad cum dignissimos
                            eos fugiat numquam pariatur possimus, qui ratione rerum soluta temporibus vel veniam.
                        </Typography>
                    </Container>
                </div>
                <Container className={classes.cardGrid} maxWidth="md">
                    <Grid
                        container
                        direaction="row"
                        spacing={3}
                        className={classes.container}
                        justify="center"
                        alignItems="center"
                    >
                        {teachers.map((teacher) => (
                            // let image = require(`../../uploads/${teacher.teachImage}`);
                            <Grid className={classes.shadow} key={teacher._id} xs={12} sm={6} md={4} item>
                                <Fade>
                                    <Card className={classes.root}>
                                        <CardHeader
                                            avatar={
                                                <Avatar aria-label="recipe" className={classes.avatar}>
                                                    R
                                                </Avatar>
                                            }
                                            title={
                                                <Typography>
                                                    {teacher.teachName}
                                                </Typography>
                                            }
                                            subheader={teacher.teachPost + "  " + teacher.teachGroup}
                                            className={classes.header}
                                        />
                                        <CardMedia
                                            className={classes.media}
                                            image={randomImage}  //image.default
                                        >
                                        </CardMedia>
                                        <CardContent>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                <span className={classes.text}>Տարիքը:</span><span
                                                className={classes.text1}>{teacher.teachOld}</span>
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                <span className={classes.text}>Կրթություն:</span><span
                                                className={classes.text1}>{teacher.teachEducation}</span>
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                <span className={classes.text}> Աշխատանքային փորձ:</span><span
                                                className={classes.text1}>{teacher.teachWorktime}</span>
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                <span className={classes.text}>Հեռախոսահամար:</span><span
                                                className={classes.text1}>{teacher.teachPhone}</span>
                                            </Typography>
                                        </CardContent>
                                    </Card>
                                </Fade>
                            </Grid>
                            ))}
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default About;