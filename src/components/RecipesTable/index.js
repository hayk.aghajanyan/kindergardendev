import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import { theme } from "../theme";
import Grid from "@material-ui/core/Grid";
import Recipe from "../Recipe";
import Loader from "../Loader";

const useStyles = makeStyles({
    container: {
        backgroundColor: theme.palette.primary.light,
        boxShadow: "-1px 5px 22px -1px rgba(0,0,0,0.75)",
    },
    space: {
        padding: "3% 0"
    },
    row: {
        "& > *": {
            color: theme.palette.common.white
        }
    },
    noBorders: {
        borderBottom: "2px solid #663399"
    }
})

const RecipesTable = () => {
    const classes = useStyles();
    const [rows, setRows] = useState()

    console.log(rows)
    useEffect(() => {
        fetch('http://localhost:5000/foodlistregister')
            .then((res) => res.json())
            .then((data) => setRows(data[0]))
    },[])

    rows && console.log(Object.values(rows))

    if(!rows) {
        return <Loader />
    } else {
        function createData(name, recipe) {
            return {
                name,
                recipe,
            }
        }

        const data =  [
            createData('Երկուշաբթի', Object.values(rows)[0]),
            createData('Երեքշաբթի', Object.values(rows)[1]),
            createData('Չորեքշաբթի', Object.values(rows)[2]),
            createData('Հինգշաբթի', Object.values(rows)[3]),
            createData('ՈԻրբաթ', Object.values(rows)[4]),
        ]

        return (
            <>
                <Grid container>
                    <Grid xs={1} item/>
                    <Grid xs={10} item>
                        <TableContainer className={classes.container} component={Paper}>
                            <Table>
                                <TableBody className={classes.noBorders}>
                                    {data.map((row) => (
                                        <Recipe key={row.name} row={row}/>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid xs={1} item/>
                </Grid>
                <Grid container>
                    <Grid xs={12} className={classes.space} item/>
                </Grid>
            </>
        );
    }
}

export default RecipesTable;