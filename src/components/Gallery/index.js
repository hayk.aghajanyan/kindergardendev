import React from "react";
import { makeStyles } from "@material-ui/core";
import Carousel from "react-material-ui-carousel";
import Grid from "@material-ui/core/Grid";
import nkar1 from "../../assets/img/nkar1.jpg"
import nkar2 from "../../assets/img/nkar2.jpg"
import nkar3 from "../../assets/img/nkar3.jpg"
import nkar4 from "../../assets/img/nkar4.jpg"
import nkar5 from "../../assets/img/nkar5.jpg"
import nkar6 from "../../assets/img/nkar6.jpg"
import nkar7 from "../../assets/img/nkar7.jpg"
import config from "../../config"
import Slider from "../Slider";

const galleryPictures = [
    {
        src: nkar1,
        id: 1
    },
    {
        src: nkar2,
        id: 2
    },
    {
        src: nkar3,
        id: 3
    },
    {
        src: nkar4,
        id: 4
    },
    {
        src: nkar5,
        id: 5
    },
    {
        src: nkar6,
        id: 6
    },
    {
        src: nkar7,
        id: 7
    }
]

const useStyles = makeStyles({
    slider: {
        margin: "2% auto"
    },
    image: {
        maxWidth: "100%"
    }
})

const Gallery = () => {
    const classes = useStyles();

    return (
        <>
            <Grid container>
                <Grid xs={1} sm={2} item />
                <Grid xs={10} sm={8} container item>
                    <Carousel className={classes.slider} navButtonsAlwaysVisible={true}>
                        {
                            galleryPictures.map((item) => {
                                return <img className={classes.image} src={item.src} alt="#" key={item.id}/>
                            })
                        }
                    </Carousel>
                    {/*<Slider items={galleryPictures} />*/}
                </Grid>
                <Grid xs={1} sm={2} item />
            </Grid>
        </>
    )
}

export default Gallery;