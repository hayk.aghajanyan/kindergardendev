import first from "../../src/assets/img/1jpg.jpg"
import second from "../../src/assets/img/2jpg.jpg"
import third from "../../src/assets/img/3jpg.jpg"
import fourth from "../../src/assets/img/4jpg.jpg"
import fifth from "../../src/assets/img/5jpg.jpg"
import sixth from "../../src/assets/img/6jpg.jpg"
import seventh from "../../src/assets/img/7jpg.jpg"
import eighth from "../../src/assets/img/8jpg.jpg"
import ninth from "../../src/assets/img/9jpg.jpg"

export const images = [ first, second, third, fourth, fifth, sixth, seventh, eighth, ninth]