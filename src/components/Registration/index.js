import React from 'react';
import {connect} from "react-redux";
import {Field, formValueSelector, reduxForm, reset} from "redux-form";
import classNames from "classnames";
import {Typography, Grid, makeStyles} from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress"
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from '@material-ui/lab/Alert';
import {theme} from "../theme";
import {CLEAR_ERROR, SET_ERROR, SET_SUCCESS} from "../../redux/actions/errorRegistration";
import {submitHandler} from "../../hooks/request.hook";
import {required, email, number} from "../../helpers/validations";
import "react-toastify/dist/ReactToastify.css";
import "../../assets/styles/registration.css";

const useStyles = makeStyles({
    container: {
        width: "100%",
        margin: "0 auto",
    },
    containerWrapper: {
        width: "100%",
        minHeight: "100vh",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        background: "#f2f2f2",
    },
    formWrapper: {
        width: 390,
        background: "#fff",
        borderRadius: 10,
        overflow: "hidden",
        padding: "77px 55px 33px 55px",
        boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.1)",
    },
    loader: {
        color: theme.palette.primary.light,
        position: "fixed",
        top: "50%",
        right: "50%"
    }
})

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Input = ({meta: {touched, error}, placeholder, input}) => {
    return (
        <div
            className={classNames("wrap-input100 validate-input", touched && " has-val", touched && error && " error")}>
            <input
                className="input100"
                type="text"
                {...input}
            />
            <span className="focus-input100" data-placeholder={placeholder}> </span>
        </div>
    )
}

const Registration = ({handleSubmit, onSubmit, submitting, pristine, success, dispatch, submitSucceeded}) => {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 791px)");
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        dispatch(CLEAR_ERROR());
    };

    if(submitSucceeded) {
        dispatch(reset("register"))
        dispatch(SET_SUCCESS());
    }

    return (
        <Grid container>
            <div className={classes.container}>
                <div className={classes.containerWrapper}>
                    {
                        !matches &&
                        <Grid xs={5} item>
                            <Typography variant="h5" style={{marginLeft: "10%"}}>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio ipsum, libero
                                minima
                                nulla
                                saepe voluptatem!
                            </Typography>
                            <Snackbar open={success} autoHideDuration={3000} onClose={handleClose}>
                                <Alert onClose={handleClose} severity="success">
                                    Ձեր երեխան գրանցված է!
                                </Alert>
                            </Snackbar>
                        </Grid>
                    }
                    {
                        !matches &&
                        <Grid xs={7} justify="center" container item>

                            <div className="wrap-login100 m-20">
                                {submitting && <CircularProgress className={classes.loader}/>}
                                <form onSubmit={handleSubmit(onSubmit)} className="login100-form validate-form">
                                    <Typography variant="h4" className="login100-form-title p-b-16">
                                        Register your child
                                    </Typography>
                                    <span className="login100-form-title p-b-28"> </span>

                                    <Field placeholder="name surname" name="kidName" component={Input}
                                           validate={required}/>
                                    <Field placeholder="parent name surname" name="parentName" component={Input}
                                           validate={required}/>
                                    <Field placeholder="age" name="age" component={Input} validate={required}/>
                                    <Field placeholder="phone" name="phoneNumber" component={Input}
                                           validate={[required, number]}/>
                                    <Field placeholder="address" name="address" component={Input} validate={required}/>
                                    <Field placeholder="email" name="email" component={Input}
                                           validate={[required, email]}/>

                                    <div className="container-login100-form-btn">
                                        <div className="wrap-login100-form-btn">
                                            <div className="login100-form-bgbtn"> </div>
                                            <button type="submit"
                                                    disabled={submitting || pristine}
                                                    className="login100-form-btn">
                                                Register
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </Grid>
                    }
                    {
                        matches &&
                        <Grid container direction={'column'} alignItems={'center'}>
                            <Grid xs={10} item>
                                {submitting && <CircularProgress className={classes.loader}/>}
                                <Typography variant="h5" style={{marginTop: "7%", textAlign: "center"}}>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio ipsum, libero
                                    minima
                                    nulla
                                    saepe voluptatem!
                                </Typography>
                            </Grid>
                            <Grid xs={10} justify="center" container item>
                                <div className="wrap-login100 m-20">
                                    <form onSubmit={handleSubmit(onSubmit)} className="login100-form validate-form">
                                        <Typography variant="h4" className="login100-form-title p-b-16">
                                            Register your child
                                        </Typography>
                                        <span className="login100-form-title p-b-28"> </span>

                                        <Field placeholder="name surname" name="kidName" component={Input}
                                               validate={required}/>
                                        <Field placeholder="parent name surname" name="parentName" component={Input}
                                               validate={required}/>
                                        <Field placeholder="age" name="age" component={Input} validate={required}/>
                                        <Field placeholder="phone" name="phoneNumber" component={Input}
                                               validate={[required, number]}/>
                                        <Field placeholder="address" name="address" component={Input}
                                               validate={required}/>
                                        <Field placeholder="email" name="email" component={Input}
                                               validate={[required, email]}/>

                                        <div className="container-login100-form-btn">
                                            <div className="wrap-login100-form-btn">
                                                <div className="login100-form-bgbtn"> </div>
                                                <button type="submit"
                                                        disabled={submitting || pristine}
                                                        className="login100-form-btn">
                                                    Register
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </Grid>
                        </Grid>
                    }
                </div>
            </div>
        </Grid>
    );
}

const mapStateToProps = (state) => {
    const selector = formValueSelector("register");
    const {success} = state.errorRegistration
    const values = selector(state, "age");
    return {
        values,
        success
    };
}

export default reduxForm({
    form: "register",
    onSubmit: async ({kidName, parentName, age, address, email, phoneNumber}) => {
        await submitHandler('/kidregister', 'POST', {"Content-Type": "application/json"}, {
            kidName,
            parentName,
            age,
            address,
            email,
            phoneNumber
        })
    },
})(connect(mapStateToProps)(Registration))