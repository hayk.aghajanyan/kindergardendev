import React from "react";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import PhoneIcon from '@material-ui/icons/Phone';
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import { Grid } from "@material-ui/core";
import { theme } from "../theme";
import { makeStyles } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles({
    whitened: {
        color: theme.palette.common.white,
    },
    facebook: {
        color: "#3b5998",
        borderRadius: "10px",
        backgroundColor: theme.palette.common.white,
    },
    insta: {
        background: "linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%)",
        // backgroundColor: theme.palette.primary.light,
        borderRadius: "10px",
        color: theme.palette.common.white
    }
})

const contactsInfo = [
    {
        icon: "facebook",
        text: "www.facebook.com"
    },
    {
        icon: "instagram",
        text: "www.instagram.com"
    },
    {
        icon: "phone number",
        text: "077-19-88-88, 010-62-80-88"
    }
];

const ContactsListItem = () => {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 1204px)");

    return (
        <div>
            {
                contactsInfo.map((item) => {
                    return (
                        <Grid key={item.icon} container>
                            <Grid xs={1} sm={matches ? 4 : 1} item/>
                            <Grid xs={10} sm={matches ? 5 : 11} key={item.icon} item>
                                <ListItem >
                                    <ListItemIcon>
                                        {
                                            item.icon === "facebook" && <FacebookIcon className={classes.facebook}/>
                                        }
                                        {
                                            item.icon === "instagram" && <InstagramIcon className={classes.insta}/>
                                        }
                                        {
                                            item.icon === "phone number" && <PhoneIcon className={classes.whitened}/>
                                        }
                                    </ListItemIcon>
                                    <a style={{textDecoration: "none"}} className={classes.whitened} href={item.text}>
                                        <ListItemText  primary={item.text} />
                                    </a>

                                </ListItem>
                            </Grid>
                            <Grid xs={1} sm={matches ? 3 : false} item/>
                        </Grid>
                    )
                })
            }
        </div>
    )
}

export default ContactsListItem;