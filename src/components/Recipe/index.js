import React, { useState } from "react";
import { theme } from "../theme";
import { makeStyles } from "@material-ui/core/styles";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import Collapse from "@material-ui/core/Collapse";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";


const useStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
            color: theme.palette.common.white
        },
    },
    cell: {
        paddingBottom: 0,
        paddingTop: 0
    },
    days: {
        fontSize: "22px",
    },
    bold: {
        fontSize: 19,
        fontWeight: "bold",
        color: theme.palette.common.white
    },
    golden: {
        "& > *": {
            fontSize: 16,
            color: theme.palette.background.default,
        },
    },

    cardMedia: {
        padding: '25%', // 16:9 56.25%
    },
    cardContent: {
        flexGrow: 1,
    },
});

const Recipe = ({row}) => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    console.log(row)

    return (
        <>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row"
                                style={{color: theme.palette.background.default}}
                                size="small"
                                onClick={() => setIsOpen(!isOpen)}
                    >
                        {isOpen ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row" className={classes.days}>
                    {row.name}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell className={classes.cell} colSpan={6}>
                    <Collapse in={isOpen} timeout="auto" unmountOnExit>
                        <Box margin={4}>
                            <Typography variant="h6"
                                        component="div"
                                        style={{color: theme.palette.background.default}}
                                        gutterBottom>
                                Օրվա ճաշացանկը
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell className={classes.bold} align={'center'}> </TableCell>
                                        <TableCell className={classes.bold} align={'center'}>Առաջինը</TableCell>
                                        <TableCell className={classes.bold} align={'center'}>Հիմնական</TableCell>
                                        <TableCell className={classes.bold} align={'center'}>Ըմպելիք</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {Object.values(row.recipe).map((currentRow) => (
                                        <TableRow className={classes.golden} key={currentRow.main}>
                                            <TableCell style={{color: theme.palette.common.white, fontSize: 17}} align={'center'} component="th" scope="row">
                                                {currentRow.mealType}
                                            </TableCell>
                                            <TableCell align={'center'} component="th" scope="row">
                                                {currentRow.main}
                                             </TableCell>
                                            <TableCell align={'center'} component="th" scope="row">
                                                {currentRow.secondary}
                                            </TableCell>
                                            <TableCell align={'center'} component="th" scope="row">
                                                {currentRow.drink}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

export default Recipe