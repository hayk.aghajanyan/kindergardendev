import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { theme } from "../theme";

const useStyles = makeStyles({
    footer: {
        backgroundColor: theme.palette.primary.dark,
        padding: "20px 0",
        height: 70,
        textAlign: "center",
    }
})

const Footer = () => {
    const classes = useStyles();

    return (
        <footer className={classes.footer}>
            <Typography>
                Ավան, Իսահակյան 10 ©
                <Link to="/about">H&A inc </Link>
                2020
            </Typography>
        </footer>
    )
}

export default Footer;