import React, { useEffect } from "react";
import MainFeaturedPost from "../MainFeaturedPost";
import CartHome from "../CartHome";
import ContactInfo from "../ContactInfo";
import catty from "../../assets/img/catty.jpg";
import doggy from "../../assets/img/thug-life-dogg.png"
import Divider from "@material-ui/core/Divider";
import Gallery from "../Gallery";
import News from "../News";

const newsInfo = [
    {
        title: "Հարմարավետ ",
        content: "Ձեր իսկ ցանկությամբ ապահովում ենք երկկողմանի փոխադրամիջոցով (Mercedes Sprinter) +15.000 դրամ",
        right: true,
        image: catty,
    },
    {
        title: "Ապահովություն",
        content: "Մեզ մոտ միացված են տեսանկարահանող սարքեր, որոնք հնարավորություն կտան հետևել փոքրիկին Ձեր ցանկացած պահին",
        right: true,
        image: catty,
    },
    {
        title: "Համով ճաշացանկ",
        content: "Ունենք տեղում պատրաստվող թարմ սնունդ - Նախաճաշ - Ճաշ- Ետճաշ - Միրգ, քաղցրավենիք",
        left: true,
        image: doggy,
        link: "/special"
    },
]

const Home = () => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <>
        <MainFeaturedPost />
        {
            newsInfo.map((item) => {
                return (
                    <div style={{backgroundImage: "url('https://w3.chabad.org/media/images/1020/cxGh10202712.jpg')"}}  key={item.title}>
                        <CartHome {...item} />
                    </div>
                )
            })
        }
        <News />
        <Gallery />
        <Divider style={{height: "1px", backgroundColor: "#000000"}} />
        <ContactInfo />
        </>
    )
}

export default Home;