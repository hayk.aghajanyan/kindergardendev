import React from "react";
import {Grid, Typography} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles({
    errorWithPad: {
        paddingTop: "1%"
    },
    errorMsg: {
        fontWeight: "bold",
    },
    background: {
        height: "80vh",
        background: 'url("https://w3.chabad.org/media/images/1020/cxGh10202712.jpg")',
    },
    monkey: {
        margin: "0 auto",
        paddingTop: "7%",
        textAlign: "center"
    },
    pd15: {
        paddingTop: "15%"
    }
})

const ErrorPage = () => {
    const classes = useStyles();
    const matches = useMediaQuery("(max-width: 655px)");
    const smPhoneMatches = useMediaQuery("(max-width: 360px)");

    return (
        <div className={classes.background}>
            <Grid className={matches ? `${classes.monkey} ${classes.pd15}` : `${classes.monkey}`} item>
                <img src="https://www.gstatic.com/youtube/src/web/htdocs/img/monkey.png" alt="#"/>
            </Grid>
            <Grid className={classes.errorWithPad} item>
                <Typography align="center"
                            variant={matches ? smPhoneMatches ? "h4" : "h3" : "h2"}
                            color="secondary"
                            className={classes.errorMsg}>
                    Error 404: This page isn't available.
                </Typography>
                <Typography align="center"
                            variant={matches ? smPhoneMatches ? "h4" : "h3" : "h2"}
                            color="secondary"
                            className={classes.errorMsg} >
                    Sorry about that.
                </Typography>
            </Grid>
        </div>

    )
}

export default ErrorPage;