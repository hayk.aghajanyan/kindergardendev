import React, {useEffect, useState} from "react";
import { Typography } from "@material-ui/core";
import { fetcher} from "../../helpers/fetcher";
import Loader from "../Loader";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {theme} from "../theme";
import image from "../../assets/img/soup.jpg"

const useStyles = makeStyles({
    container: {
        display: "flex",
        flex: 1,
        backgroundColor: theme.palette.primary.light,
        justifyContent: "center",
        height: "600px"

    },
    wrapper: {
        height: "100%",
        width: "100%",
        textAlign: "center",
    },
    p: {
        color: "#fff",
        fontSize: 15
    },
    title: {
        color: "#fff",
        fontSize: 17
    },
    img: {
        marginTop: "3%",
        width: "40%",
        height: "50%"
    }
})

const News = () => {
    const [news, setNews] = useState()
    const classes = useStyles()

    useEffect(() => {
        fetcher('/occasionregister').then((data) => setNews(data[0]))
    }, [])
    if(!news) {
        return <Loader />
    } else {
        return (
            <div className={classes.container}>
                <div className={classes.wrapper}>
                    <img className={classes.img} src={news.imgName} alt="#"/>
                    <Typography className={classes.title}>{news.occasionTime}</Typography>
                    <Typography className={classes.p}>{news.occasionText}</Typography>
                </div>
            </div>
        )
    }
}

export default News;