import { useState, useCallback } from 'react';
import config from '../config'

export const useRequest = () => {
    const [loading, setLoading] = useState(false),
          [error, setError] = useState(null);

    const request = useCallback(async (url, method = 'GET', body = null, headers = {}) => {
        setLoading(true);
        try {
            if(body) {
                body = JSON.stringify(body);
                headers['Content-Type'] = 'application/json';
            }
            const response = await fetch(`${config.apiUrl}${url}`, {method, body, headers});
            const data = await response.json();

            if(!response.ok) {
                throw new Error(data.message || 'Oops, cannot get good response...')
            }

            setLoading(false);
            return data;
        } catch (e) {
            setLoading(false);
            setError(e.message);
            throw e;
        }
    }, [])

    const clearError = () => setError(null);

    return { request, loading, error, clearError }
}

export const submitHandler = (url, method, headers, body) => {
    const options = {
        method,
        headers,
        body: JSON.stringify({
            kidName: body.kidName,
            parentName: body.parentName,
            age: body.age,
            address: body.address,
            phoneNumber: body.phoneNumber,
            email: body.email,
            status: "applicant"
        }),
    };
    fetch(`${config.apiUrl}${url}`, options)
        .then((data) => data.json())
        .then((jsonData) => {
            console.log(">>>>>>>  ", jsonData);
        });
};