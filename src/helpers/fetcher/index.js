import config from "../../config"

export const fetcher = (url) => {
    return fetch(`${config.apiUrl}${url}`)
        .then((res) => res.json())
}