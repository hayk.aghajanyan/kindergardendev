import config from '../../config'

export const fetchedRecipes = () => {
    const recipes = fetch(`${config.apiUrl}/foodlistregister`)
        .then((res) => res.json())
        .then((data) => data[1])
    function createData(name, recipe) {
        return {
            name,
            recipe,
        }
    }

    return [
        createData('Երկուշաբթի', recipes.monday),
        createData('Երեքշաբթի', recipes.tuesday),
        createData('Չորեքշաբթի', recipes.wednesday),
        createData('Հինգշաբթի', recipes.thursday),
        createData('ՈԻրբաթ', recipes.friday),
    ]
}