export const required = (value) => (value ? undefined : "Field is required");

export const email = (value) =>
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Invalid email address" : undefined;

export const number = (value) =>
    // eslint-disable-next-line
    !/^([\d\s\(\)+-]{8,18})$/i.test(value) ? "Wrong number" : undefined;
