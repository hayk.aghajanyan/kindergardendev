import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Header from "./components/Header";
import Home from "./components/Home";
import About from "./components/About";
import Special from "./components/Special";
import Footer from "./components/Footer";
import ErrorPage from "./components/ErrorPage";
import Registration from "./components/Registration";

function App() {
    return (
        <>
            <Header/>
            <Switch>
                <Route path="/" component={Home} exact />
                <Route path="/special" component={Special} exact />
                <Route path="/registration" component={Registration} exact />
                <Route path="/about" component={About} exact />
                <Route component={ErrorPage} />
            </Switch>
            <Footer/>
        </>
    );
}

export default App;
